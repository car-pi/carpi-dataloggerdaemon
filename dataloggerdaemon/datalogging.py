import logging
from logging import LogRecord


class ContextFilter(logging.Filter):
    def __init__(self, context: str):
        super().__init__()
        self._match_context = context

    def filter(self, record: LogRecord) -> bool:
        context = record.name
        matches_context = context == self._match_context
        if matches_context:
            if not hasattr(record, 'channel'):
                record.channel = ''
            if not hasattr(record, 'value'):
                record.value = 'null'
        return matches_context


class InverseContextFilter(ContextFilter):
    def __init__(self, context: str):
        super().__init__(context)

    def filter(self, record: LogRecord) -> bool:
        return not super().filter(record)


LOG_CONFIG = dict(
    version=1,
    formatters={
        'default': {
            'format': '[%(asctime)s.%(msecs)03d] [%(name)-15s] %(levelname)-8s %(message)s',
            'datefmt': '%Y-%m-%d %H:%M:%S'
        },
        'data': {
            'format': '%(asctime)s.%(msecs)03d|%(channel)s|%(value)s',
            'datefmt': '%Y-%m-%dT%H:%M:%S'
        }
    },
    handlers={
        'stdout': {
            'class': 'logging.StreamHandler',
            'formatter': 'default',
            'level': logging.DEBUG,
            'filters': ['notdatafilter']
        }
    },
    filters={
        'datafilter': {
            '()': ContextFilter,
            'context': 'DATA'
        },
        'notdatafilter': {
            '()': InverseContextFilter,
            'context': 'DATA'
        }
    },
    root={
        'handlers': ['stdout'],
        'level': logging.INFO
    }
)
