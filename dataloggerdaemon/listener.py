from time import sleep
from typing import Optional

from redis.client import PubSub
from redisdatabus.bus import BusListener


class DataListener(BusListener):
    def __init__(self,
                 sub_pattern: str,
                 name: str = None,
                 host: str = '127.0.0.1',
                 port: int = 6379,
                 db: int = 0,
                 password: Optional[str] = None):
        super().__init__([],
                         name,
                         None,
                         host,
                         port,
                         db,
                         password)
        self._sub_pattern = sub_pattern

    def _init_listener(self) -> PubSub:
        sub = self._r.pubsub()
        sub.psubscribe(self._sub_pattern)
        return sub

    def run(self) -> None:
        self._log.info("Starting up...")
        self._running = True

        sub = self._init_listener()
        while self._running:
            msg = sub.get_message(ignore_subscribe_messages=True,
                                  timeout=1)
            if msg and msg['type'] == 'pmessage':
                channel, data = self._process_entry(msg)
                self._current_data[channel] = data
                for glc in self._global_callbacks:
                    glc(channel, data)
                if channel in self._callbacks:
                    for cl in self._callbacks[channel]:
                        cl(channel, data)
            sleep(0.001)
