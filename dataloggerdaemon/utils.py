import os


def disk_exists(path: str) -> bool:
    try:
        os.stat(path)
        return True
    except OSError:
        return False


def disk_mounted(path: str) -> bool:
    try:
        return os.path.ismount(path)
    except OSError:
        return False
