from daemoncommons.daemon import DaemonRunner

from dataloggerdaemon.daemon import DataLoggerDaemon

if __name__ == '__main__':
    d = DaemonRunner('DATALOGGERD_CFG', ['dataloggerd.ini', '/etc/carpi/dataloggerd.ini'])
    d.run(DataLoggerDaemon())
