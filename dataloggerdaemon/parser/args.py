from argparse import ArgumentParser


def build_command_line_parser() -> ArgumentParser:
    p = ArgumentParser(description='Processes a log file written by dataloggerdaemon')
    p.add_argument('input_file', type=str, nargs='+',
                   help='File to process')
    p.add_argument('--output_file', type=str,
                   help='File to output to')
    p.add_argument('--split_dir', type=str,
                   help='Directory to store split files to')
    return p
