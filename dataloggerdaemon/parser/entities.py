from json import dumps
from typing import Any, List

import isodate


TYPE_STRING = 'str'
TYPE_FLOAT = 'float'
TYPE_INT = 'int'
TYPE_DICT = 'dict'
TYPE_OTHER = 'other'


class LogRecord(object):
    CSV_HEADER = [
        'Timestamp',
        'Key',
        'Value'
    ]

    def __init__(self,
                 timestamp: str,
                 key: str,
                 value: Any):
        self.timestamp = isodate.parse_datetime(timestamp)
        self.key = key
        self.value = value

    def get_csv_row(self) -> List[str]:
        return [
            self.timestamp.isoformat(),
            self.key,
            self.value
        ]

    def __str__(self) -> str:
        return '{}: {}'.format(self.key, self.value)

    def __repr__(self) -> str:
        return '{}: {}'.format(self.key, self.value)
