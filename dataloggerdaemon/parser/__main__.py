import csv
import os.path
from json import loads
from json.decoder import JSONDecodeError
from typing import List, Dict

from dataloggerdaemon.parser.args import build_command_line_parser
from dataloggerdaemon.parser.entities import LogRecord
from dataloggerdaemon.parser.log import setup_logging


def read_log_file(paths: List[str]) -> List[LogRecord]:
    from dataloggerdaemon.parser.log import LOG
    LOG.info('Reading %.0f log files ...', len(paths))
    all_records = []

    for path in paths:
        LOG.info('Reading %s ...', path)
        filename = os.path.basename(path)

        records = []
        with open(path, mode='r') as f:
            csv_file = csv.reader(f, delimiter='|', quotechar="'")
            line_no = 0
            for line in csv_file:
                line_no += 1
                LOG.debug('%s: Reading line %.0f', filename, line_no)
                if len(line) < 3:
                    LOG.warning('%s:%.0f : Line did not contain enough fields, skipped', filename, line_no)
                    continue

                try:
                    timestamp = line[0]
                    key = line[1]
                    value = loads(line[2])
                except JSONDecodeError as e:
                    LOG.error('%s:%.0f : Error decoding value in JSON format: %s (Value was: %s)',
                              filename, line_no, e, str(line[2]))
                    continue
                records.append(LogRecord(timestamp, key, value))

        LOG.info('%s : Read %.0f objects', filename, len(records))
        all_records += records

    LOG.info('Read all %.0f files, got %.0f records', len(paths), len(all_records))
    return all_records


def write_to_csv(records: List[LogRecord],
                 path: str):
    from dataloggerdaemon.parser.log import LOG
    LOG.info('Writing %.0f records to "%s" ...', len(records), path)
    rows = list(map(lambda r: r.get_csv_row(), records))
    with open(path, mode='w', newline='') as f:
        csv_file = csv.writer(f)
        csv_file.writerow(LogRecord.CSV_HEADER)
        csv_file.writerows(rows)
    LOG.info('Done!')


def find_all_files(paths: List[str]) -> List[str]:
    from glob import glob
    all_files = []
    for path in paths:
        all_files += glob(path)
    return all_files


if __name__ == '__main__':
    parser = build_command_line_parser()
    args = parser.parse_args()

    setup_logging()
    records = read_log_file(find_all_files(args.input_file))
    if args.output_file:
        write_to_csv(records, args.output_file)
    elif args.split_dir:
        from dataloggerdaemon.parser.log import LOG
        LOG.info('Creating item catalogue ...')
        d: Dict[str, List[LogRecord]] = dict()
        for r in records:
            if r.key not in d:
                d[r.key] = []
            d[r.key].append(r)

        for k, l in d.items():
            write_to_csv(l, os.path.join(args.split_dir, '{}.csv'.format(k)))
