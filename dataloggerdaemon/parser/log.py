import logging
from sys import stdout
from typing import Optional

from carpicommons.log import configure_logging, logger

LOG: Optional[logging.Logger] = None


PARSER_LOG_CONFIG = dict(
    version=1,
    formatters={
        'default': {
            'format': '[%(asctime)s.%(msecs)03d] [%(name)-15s] %(levelname)-8s %(message)s',
            'datefmt': '%Y-%m-%d %H:%M:%S'
        }
    },
    handlers={
        'stdout': {
            'class': 'logging.StreamHandler',
            'formatter': 'default',
            'level': logging.DEBUG
        }
    },
    filters={},
    root={
        'handlers': ['stdout'],
        'level': logging.INFO
    }
)


def setup_logging():
    global LOG
    configure_logging([], dict_config=PARSER_LOG_CONFIG)
    LOG = logger('Parser')
