import json
from datetime import datetime
from logging import Logger, INFO, DEBUG
from time import sleep
from typing import Optional, List

from carpicommons.log import logger, configure_logging
from daemoncommons.daemon import Daemon
from redisdatabus.bus import BusWriter

from dataloggerdaemon.datalogging import LOG_CONFIG
from dataloggerdaemon.listener import DataListener
from dataloggerdaemon.utils import disk_exists, disk_mounted

KEY_HEARTBEAT = 's#carpi.logd.ts'
KEY_IS_LOGGING = 'b#carpi.logd.is_logging'

SELF_LOG = [
    KEY_HEARTBEAT,
    KEY_IS_LOGGING
]


class DataLoggerDaemon(Daemon):
    def __init__(self):
        super().__init__('DataLoggerD')
        self._log: Optional[Logger] = None
        self._data_logger: Optional[Logger] = None
        self._bus: Optional[DataListener] = None
        self._heartbeat_bus: Optional[BusWriter] = None
        self._running = False

        self._is_log_device_present = False
        self._is_log_device_mounted = False

    def _configure_logging(self):
        LOG_CONFIG['handlers']['datalog'] = {
            'class': 'logging.handlers.TimedRotatingFileHandler',
            'formatter': 'data',
            'filters': ['datafilter'],
            'level': INFO,
            'filename': self._get_config('Logging', 'TargetPath'),
            'when': self._get_config('Logging', 'RolloverWhen', 'H'),
            'utc': True,
            'delay': True,
        }
        root_handlers: List[str] = LOG_CONFIG['root']['handlers']
        if 'datalog' not in root_handlers:
            root_handlers.append('datalog')
        configure_logging([],
                          dict_config=LOG_CONFIG)

    def _build_bus_writer(self) -> BusWriter:
        self._log.info("Connecting to Redis instance ...")
        return BusWriter(host=self._get_config('Redis', 'Host', '127.0.0.1'),
                         port=self._get_config_int('Redis', 'Port', 6379),
                         db=self._get_config_int('Redis', 'DB', 0),
                         password=self._get_config('Redis', 'Password', None))

    def _build_bus_reader(self) -> DataListener:
        self._log.info('Connecting to Data Source Redis instance ...')
        return DataListener(sub_pattern=self._get_config('Logger', 'Pattern', '?#carpi.*'),
                            host=self._get_config('Redis', 'Host', '127.0.0.1'),
                            port=self._get_config_int('Redis', 'Port', 6379),
                            db=self._get_config_int('Redis', 'DB', 0),
                            password=self._get_config('Redis', 'Password', None))

    def _on_command_received(self,
                             channel: str,
                             value: str):
        if self._is_log_device_present \
                and self._is_log_device_mounted \
                and channel not in SELF_LOG:
            self._data_logger.info('DATA', extra={
                'channel': channel,
                'value': json.dumps(value)
            })

    def startup(self):
        self._log = log = logger(self.name)
        self._data_logger = logger('DATA')
        log.info('Starting up Command Daemon ...')

        self._bus = bus = self._build_bus_reader()
        self._heartbeat_bus = heartbeat = self._build_bus_writer()
        bus.register_global_callback(lambda c, v: self._on_command_received(c, v))

        log_device = self._get_config('Logging', 'LogIfDrivePresent', '')
        log_mount = self._get_config('Logging', 'LogIfDriveMountedAt', '')
        self._check_log_device_presence(log_device)
        self._check_log_device_mounted(log_mount)

        log.info('Startup complete! Entering main loop.')

        bus.start()
        self._running = True
        while self._running:
            self._check_log_device_presence(log_device)
            self._check_log_device_mounted(log_mount)

            heartbeat.publish(KEY_HEARTBEAT, datetime.now().isoformat())
            heartbeat.publish(KEY_IS_LOGGING, int(self._is_log_device_present and self._is_log_device_mounted))
            sleep(5)

    def _check_log_device_presence(self,
                                   log_device: str):
        self._is_log_device_present = not log_device or disk_exists(log_device)
        self._log.info('Checked presence of log device "%s" -> %s',
                       log_device,
                       self._is_log_device_present)

    def _check_log_device_mounted(self,
                                  log_device: str):
        self._is_log_device_mounted = not log_device or disk_mounted(log_device)
        self._log.info('Checked presence of mount for log device "%s" -> %s',
                       log_device,
                       self._is_log_device_mounted)

    def shutdown(self):
        if self._log:
            self._log.info('Shutting down Command Daemon ...')
        if self._bus:
            self._bus.stop()
            self._bus.join()
        super().shutdown()
