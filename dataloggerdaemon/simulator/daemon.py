import csv
from datetime import datetime
from json import JSONDecodeError, loads
from logging import Logger
from time import sleep
from typing import Optional

from carpicommons.log import logger
from daemoncommons.daemon import Daemon
from redisdatabus.bus import BusWriter


KEY_IS_SIMULATION = 'b#carpi.simulation.is_active'
KEY_SIMULATION_TS = 's#carpi.simulation.ts'

MODE_INTERVAL = 'Interval'
MODE_REALTIME = 'Realtime'


class SimulatorDaemon(Daemon):
    def __init__(self):
        super().__init__('SimulatorD')
        self._log: Optional[Logger] = None
        self._bus: Optional[BusWriter] = None
        self._running = False

    def _build_bus_writer(self) -> BusWriter:
        self._log.info("Connecting to Redis instance ...")
        return BusWriter(host=self._get_config('Redis', 'Host', '127.0.0.1'),
                         port=self._get_config_int('Redis', 'Port', 6379),
                         db=self._get_config_int('Redis', 'DB', 0),
                         password=self._get_config('Redis', 'Password', None))

    def startup(self):
        self._log = log = logger(self.name)

        self._bus = bus = self._build_bus_writer()

        log.info('Reading configuration ...', )
        input_file_path = self._get_config('Simulation', 'InputFile')
        sim_mode = self._get_config('Simulation', 'Mode', MODE_INTERVAL)
        replace_systimestamp = self._get_config_bool('Simulation', 'ReplaceSystimestamp', False)
        interval = self._get_config_float('Simulation.Interval', 'Interval', 0.1)

        self._running = True
        log.info('Opening file "%s" ...', input_file_path)
        with open(input_file_path, 'r') as input_file:
            log.info('Starting simulation ...')
            while self._running:
                csv_file = csv.reader(input_file, delimiter='|', quotechar="'")
                line_no = 0
                for line in csv_file:
                    line_no += 1
                    if not self._running:
                        log.warning('Daemon stop requested, stop reading file ...')
                        break
                    if len(line) < 3:
                        log.warning('Line %.0f did not contain enough fields, skipped', line_no)
                        continue

                    try:
                        timestamp = line[0]
                        key = line[1]
                        value = loads(line[2])
                    except JSONDecodeError as e:
                        log.error('Error decoding value in JSON format on line %.0f: %s (Value was: %s)',
                                  line_no, e, str(line[2]))
                        continue

                    orig_value = None
                    if replace_systimestamp:
                        if 'systimestamp' in key:
                            orig_value = value
                            new_value = datetime.now().isoformat()
                            log.info('Replacing timestamp "%s" (old value="%s", new value="%s")',
                                     key, orig_value, new_value)
                            value = new_value

                    if sim_mode == MODE_INTERVAL:
                        log.info('Publishing "%s" = "%s"', key, value)
                        bus.publish(key, value)
                        if orig_value:
                            bus.publish('{}.orig'.format(key), orig_value)
                        sleep(interval)
                    elif sim_mode == MODE_REALTIME:
                        log.error('Realtime Mode is not implemented yet')

                    self._send_heartbeat()

                # When file is done, stop the daemon
                self._running = False

            log.info('Closing input file ...')

        bus.publish(KEY_IS_SIMULATION, '0')

    def _send_heartbeat(self):
        self._log.debug('Sending heartbeat ...')
        bus = self._bus
        if not bus:
            return
        bus.publish(KEY_IS_SIMULATION, '1')
        bus.publish(KEY_SIMULATION_TS, datetime.now().isoformat())

    def shutdown(self):
        if self._log:
            self._log.info('Shutting down Command Daemon ...')
        self._running = False
