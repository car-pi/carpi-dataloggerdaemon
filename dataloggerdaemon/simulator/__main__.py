from daemoncommons.daemon import DaemonRunner

from dataloggerdaemon.simulator.daemon import SimulatorDaemon

if __name__ == '__main__':
    d = DaemonRunner('SIMD_CFG', ['simd.ini', '/etc/carpi/simd.ini'])
    d.run(SimulatorDaemon())
